import game_types
from storages.file_storage import FileStorage

FILE_STORAGE_SETTINGS = {
    'path': './files/'
}

STORAGE_CLS = FileStorage

from guis import simple_console_gui
GUI_MODULE = simple_console_gui
