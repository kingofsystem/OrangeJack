import hashlib
import uuid
from datetime import datetime
from enum import Enum
import random
from typing import List


class CardValues(Enum):
    ACE = 0
    TWO = 1
    THREE = 2
    FOUR = 3
    FIVE = 4
    SIX = 5
    SEVEN = 6
    EIGHT = 7
    NINE = 8
    TEN = 9
    JACK = 10
    QUEEN = 11
    KING = 12


class CardTypes(Enum):
    HEART = 0x1f0b1
    DIAMOND = 0x1f0c1
    CLUB = 0x1f0d1
    SPADE = 0x1f0a1

    def describe(self) -> (str, str):
        return self.name, chr(self.value)


class Card:

    def __init__(self, card_type: CardTypes, card_value: CardValues):
        self.type = card_type
        self.value = card_value

    @staticmethod
    def create_random_card():
        return Card(random.choice(list(CardTypes)),
                    random.choice(list(CardValues)))

    def __repr__(self):
        return '<Card: type=%s value=%s>' % (self.type, self.value)


class PlayerProfile:

    def __init__(self, username: str, password: str, money: int = 0):
        self.player_id = str(uuid.uuid4())
        self.username = username
        self.pass_hash = hashlib.sha256(password.encode()).hexdigest()
        self.money = money

    def check_pass(self, password: str):
        if self.pass_hash == hashlib.sha256(password.encode()).hexdigest():
            return True
        return False

    def __repr__(self):
        return '<PlayerProfile: username=%s money=%s>' % (self.username,
                                                          self.money)


class PlayerInGame:

    def __init__(self, player_id: str, cards=list(), bet: int = 0):
        self.player_id = player_id
        self.player_in_game_id = str(uuid.uuid4())
        self.cards = cards
        self.bet = bet

    @staticmethod
    def init_from_profile(player_profile: PlayerProfile):
        return PlayerInGame(player_profile.player_id)

    def __repr__(self):
        return 'PlayerInGame: player_id=%s bet=%s' % (self.player_id,
                                                      self.bet)


# class BotPlayer(PlayerInGame):
#     def __init__(self, cards=list(), bet: int = 0, money: int = 0):
#         super(BotPlayer, self).__init__(BOT_PLAYER_ID, cards, bet)
#         self.money = money


class SavedGame:
    """
    GameType can add custom fields to the save, if needed.
    """
    def __init__(self, settings: dict):
        self.saved_game_id = str(uuid.uuid4())
        self.settings = settings
        self.self = self
        self.date = datetime.now()

    def __repr__(self):
        return '<SavedGame: id=%s date=%s>' % (self.saved_game_id,
                                               self.date)


class PlayerResult:
    def __init__(self, player_id: str, money_spent: int, money_earn: int):
        self.result_id = str(uuid.uuid4())
        self.player_id = player_id
        self.money_spent = money_spent
        self.money_earn = money_earn

    def __repr__(self):
        return '<PlayerResult: id=%s>' % self.player_id


class GameResult:
    def __init__(self, player_results_ids: List[str]):
        self.game_id = str(uuid.uuid4())
        self.player_results_ids = player_results_ids
        self.date = datetime.now()

    def __repr__(self):
        return '<GameResult: id=%s date=%s>' % (self.game_id, self.date)
