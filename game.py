import game_types
import settings
from game_objects import PlayerProfile, GameResult
from gui import BackMessageFromLobby


class Game:
    def __init__(self):
        self.storage = settings.STORAGE_CLS()
        self.gui_module = settings.GUI_MODULE
        self.current_player = None

    def start_game(self):
        self.gui_module.WelcomeState(self.storage, self.auth_user)

    def auth_user(self):
        self.gui_module.UserAuthRegisterState(self.storage,
                                              self.enter_lobby)

    def enter_lobby(self, player_profile: PlayerProfile):
        self.current_player = player_profile
        self.gui_module.LobbyState(self.storage, self.lobby_callback)

    def lobby_callback(self, message, **kwargs):
        if message == BackMessageFromLobby.ENTER_SHOP:
            self.gui_module.ShopState(self.storage, self.return_from_shop,
                                      self.current_player)
        elif message == BackMessageFromLobby.CREATE_GAME:
            self.gui_module.CreateGameState(game_types.__all__,
                                            self.storage,
                                            self.return_from_game_creating)

    def return_from_shop(self):
        self.gui_module.LobbyState(self.storage, self.lobby_callback,
                                   False)

    def return_from_game_creating(self, game_settings):
        self.gui_module.InGameState(game_settings['game_type'],
                                    self.storage,
                                    self.exit_game,
                                    game_settings)

    def exit_game(self, game_result: GameResult):
        pass
