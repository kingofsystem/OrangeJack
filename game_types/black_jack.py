from typing import List

from game_objects import SavedGame, PlayerProfile, PlayerInGame, Card, \
    CardTypes, CardValues
from game_type import BaseGameType
import gui

_HELP = """
While game is running you can use these commands:

hit
stand
double

Game rules are here: https://en.wikipedia.org/wiki/Blackjack
""".strip()


class BlackJack(BaseGameType):

    NAME = 'Black Jack'

    SETTINGS = [
        # Format is ('param_name', type, 'Verbose name')
        ('bet', int, 'Bet'),
        ('player', PlayerProfile, 'Player'),
    ]

    def __init__(self, game_settings: dict, view: gui.InGameStateBase):
        super().__init__(game_settings, view)
        self.in_game_user = None
        self.player_cards = list()

    def restore_game(self, save: SavedGame):
        super().restore_game(save)

    def save_game(self):
        self.game_settings['croupier_cards'] = self.croupier_cards
        self.view.save_game(SavedGame(self.game_settings))

    def start_game(self):
        self.view.message('🂠🂠🂠Black Jack🂠🂠🂠')
        self.view.message('Welcome to game!')
        self.view.message(_HELP)
        self.view.get_user_command('Press Enter when you are ready')

        self.view.message('Initializing game...')
        self._give_cards_to_croupier()
        self.view.message(f'Cards of croupier is '
                          f' {"🂠"*len(self.croupier_cards)}'
                          f'Score is unavailable for now.')

        self.player_cards.append(self._get_card())
        self.player_cards.append(self._get_card())

        self.view.message(f'Your cards is '
                          f'{self._represent_cards(self.player_cards)}.')
        self.view.get_user_command('Okay?')
        self.save_game()

        self._game_loop()

        self.save_game()

        self.view.message(f'Croupier cards is '
                          f'{self._represent_cards(self.croupier_cards)}, '
                          f'score is {self._calc_score(self.croupier_cards)} '
                          f'Your score is {self._calc_score()}')


    def _game_loop(self):
        while True:
            player_choices = []
            current_score = self._calc_score(self.player_cards)
            if current_score == 21:
                self.view.message('You got a BLackJack')
                break
            if current_score > 21:
                self.view.message('Its fail.')
                break
            if current_score == 10 and current_score == 11:
                player_choices += 'double'
            if current_score < 21:
                player_choices += 'hit'
                player_choices += 'stand'

            self.save_game()
            self.view.message('')
            while player_choices not in player_choices:
                player_choice = self.view.get_user_command('Your choice: ')
                if player_choice in player_choices:
                    break

            if player_choice == 'hit':
                self.player_cards.append(self._get_card())
            elif player_choice == 'double':
                self.player_cards.append(self._get_card())
                self.game_settings['bet'] *= 2
                break
            elif player_choice == 'stand':
                break


    def _represent_cards(self, cards: List[Card]) -> str:
        result = ''
        for card in cards:
            result += chr(card.type.value +  card.value.value)
        return result

    @staticmethod
    def _get_card():
        return Card.create_random_card()

    @staticmethod
    def _calc_score(cards: List[Card]) -> int:
        score = 0
        aces = 0
        for card in cards:
            if card.value == CardValues.ACE:
                aces += 1
            else:
                score += card.value.value
        if score + 11 > 21 and aces != 0:
            score += aces
        elif aces != 0:
            score += 11 + (aces - 1)
        return score

    def _give_cards_to_croupier(self):
        # Ultra simple bot logic.
        self.croupier_cards = list()
        while self._calc_score(self.croupier_cards) < 16:
            self.croupier_cards.append(self._get_card())
