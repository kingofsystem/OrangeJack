NeonCasino
===========

My solve for test task from company. I want him look as small game engine for card games. I trying to make it self-explorable without any excess documentation. 

HowTo launch
------------

It can run without any external dependencies, but some types of GUIs or 
storages can use them. If you want to run it fast, simply use a 
simple_console_gui and file_storage. Its used by default.

To run use:

`python3 main.py`

