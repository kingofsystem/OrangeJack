from game_objects import SavedGame
from gui import InGameStateBase
from settings import GUI_MODULE


class BaseGameType:
    def __init__(self, game_settings: dict, view: InGameStateBase):
        self.game_settings = game_settings
        self.view = view

    def restore_game(self, save: SavedGame):
        raise NotImplementedError()

    def start_game(self):
        raise NotImplementedError()
