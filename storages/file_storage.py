import os
from typing import List

from db_observer import StorageBase
from game_objects import GameResult, PlayerProfile, SavedGame
import pickle


class FileStorage(StorageBase):
    def __init__(self):
        try:
            from settings import FILE_STORAGE_SETTINGS
            self._path = FILE_STORAGE_SETTINGS['path']
        except ImportError:
            self._path = './file-storage/'
        if not os.path.exists(self._path):
            os.mkdir(self._path)

        self._game_results_path = self._create_if_not_exist(
            os.path.join(self._path, 'game_results/'))
        self._profiles_path = self._create_if_not_exist(
            os.path.join(self._path, 'profiles/'))
        self._saved_games_path = self._create_if_not_exist(
            os.path.join(self._path, 'saved_game/'))

    @staticmethod
    def _create_if_not_exist(path):
        if not os.path.exists(path):
            os.mkdir(path)
        return path

    @staticmethod
    def _open_file(folder, path, mode='rb'):
        return open(os.path.join(folder, path), mode)

    def save_game(self, game_state: SavedGame):
        pickle.dump(game_state, self._open_file(self._saved_games_path,
                                                game_state.saved_game_id,
                                                mode='wb'))

    def load_saved_games(self) -> List[SavedGame]:
        result = []
        for file in os.listdir(self._saved_games_path):
            result.append(pickle.load(os.path.join(self._saved_games_path,
                                                   file)))
        return result

    def save_game_result(self, game_result: GameResult):
        pickle.dump(game_result, os.path.join(self._game_results_path,
                                              game_result.game_id))

    def load_game_results(self) -> List[GameResult]:
        result = []
        for file in os.listdir(self._game_results_path):
            result.append(pickle.load(os.path.join(self._game_results_path,
                                                   file)))
        return result

    def save_profile(self, player_profile: PlayerProfile):
        pickle.dump(player_profile, self._open_file(self._profiles_path,
                                                    player_profile.player_id,
                                                    mode='wb'))

    def get_user(self, username: str) -> PlayerProfile:
        for file in os.listdir(self._profiles_path):
            player_profile = pickle.load(self._open_file(self._profiles_path,
                                                         file))
            if player_profile.username == username:
                return player_profile
        raise ValueError('Cannot find user with this username ' + username)

    def get_user_by_id(self, user_id: str) -> PlayerProfile:
        for file in os.listdir(self._profiles_path):
            player_profile = pickle.load(os.path.join(self._profiles_path,
                                                      file))
            if player_profile.player_id == user_id:
                return player_profile
        raise ValueError('Cannot find user with this user id ' + user_id)

    def delete_user(self, profile_id: str):
        for file in os.listdir(self._profiles_path):
            if file == profile_id:
                os.remove(os.path.join(self._profiles_path, file))
                return True
        return False

    def get_all_users(self) -> List[PlayerProfile]:
        result = []
        for file in os.listdir(self._profiles_path):
            player_profile = pickle.load(os.path.join(self._profiles_path,
                                                      file))
            result.append(player_profile)
        return result
