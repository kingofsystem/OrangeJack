from enum import Enum
from typing import List, Callable, Type, Any

from db_observer import StorageBase
from game_objects import GameResult, SavedGame, PlayerProfile


class BaseState:
    def __init__(self, storage: StorageBase, callback: Callable[[], None]):
        self.storage = storage
        self.callback = callback


class WelcomeStateBase(BaseState):
    pass


class UserAuthRegisterStateBase(BaseState):
    pass


class BackMessageFromLobby(Enum):
    ENTER_SHOP = 1
    LOAD_GAME = 2
    CREATE_GAME = 3


class LobbyStateBase(BaseState):
    def __init__(self, storage: StorageBase, callback: Callable[[], None],
                 is_first_time: bool = True):
        super(LobbyStateBase, self).__init__(storage, callback)
        self.is_first_time = is_first_time


class ShopStateBase(BaseState):
    def __init__(self, storage: StorageBase,
                 callback: Callable[[], None],
                 profile: PlayerProfile):
        super(ShopStateBase, self).__init__(storage, callback)
        self.profile = profile


class StatsState(BaseState):
    pass


class CreateGameStateBase(BaseState):
    def __init__(self, game_types: List[Any],
                 storage: StorageBase,
                 callback: Callable[[dict], None]):
        super(CreateGameStateBase, self).__init__(storage, callback)
        self.game_types = game_types


class InGameStateBase:
    """
    This class realize actions, that can be called by GameType.
    If actions will not realize this action, it need to go to
    fallback realization (e. g. simple text message or modal
    window). Action likes card giving, shuffling, placing on the table, etc.
    InGameState realize only visual effects. Logic is business of GameType.
    """

    def __init__(self, game_type: Type[Any], storage: StorageBase,
                 callback: Callable[[GameResult], None],
                 game_settings: dict):
        self.game_type = game_type
        self.storage = storage
        self.callback = callback
        self.game = game_type(game_settings, self)
        # Launch the game here

    def save_game(self, saved_game: SavedGame):
        self.storage.save_game(saved_game)

    def get_user_command(self, prompt: str) -> str:
        raise NotImplementedError()

    def _fallback_action(self, action_str: str):
        raise NotImplementedError()

    def __getattr__(self, item):
        return self._fallback_action
