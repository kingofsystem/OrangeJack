import random
import time
import sys

def clrscr():
    print('\033[H\033[J')


def print_delayed(*args, file=sys.stdout, end='\n'):
    for arg in args:
        for char in arg:
            print(char, end='', file=file)
            file.flush()
            time.sleep(random.uniform(0.04, 0.08))
        print(' ', file=file, end='')
    print(end, file=file, end='')
