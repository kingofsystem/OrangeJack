from typing import Type, Callable, Any

from db_observer import StorageBase
from game_objects import GameResult
from gui import InGameStateBase
from guis.simple_console_gui.utils import clrscr, print_delayed


class InGameState(InGameStateBase):
    """
    Very simple realization.
    """
    def __init__(self, game_type: Type[Any], storage: StorageBase,
                 callback: Callable[[GameResult], None],
                 game_settings: dict):
        super(InGameState, self).__init__(game_type, storage, callback,
                                          game_settings)
        clrscr()
        self.game_instance = self.game_type(game_settings, self)
        print_delayed('Game was started. Good luck!')
        self.game_instance.start_game()

    def get_user_command(self, prompt: str):
        print_delayed(prompt, end='')
        return input()

    def _fallback_action(self, action_str: str):
        print_delayed(action_str)
