import threading
from typing import Callable

from db_observer import StorageBase
from gui import WelcomeStateBase
from guis.simple_console_gui.utils import clrscr, print_delayed


class WelcomeState(WelcomeStateBase):
    def __init__(self,storage: StorageBase,  callback: Callable[[], None]):
        super().__init__(storage, callback)
        clrscr()
        threading.Thread(target=self._start_gui).start()

    def _start_gui(self):
        print_delayed('Welcome to NeonCasino!')
        print_delayed('Press Enter.')
        input()
        self.callback()
