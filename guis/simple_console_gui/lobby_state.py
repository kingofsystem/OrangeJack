import threading
from typing import Callable

import sys

from db_observer import StorageBase
from gui import LobbyStateBase, BackMessageFromLobby
from guis.simple_console_gui.utils import print_delayed
from .utils import clrscr

_MAN = """
Welcome to the game lobby. Here you can do everything you want (with some 
small limitations). Don`t lose all your money. Good luck. But don`t believe 
in luck.
"""

_COMMANDS = """
Available commands:
help - show this help
saves - show all saved games
load <number> - load save by number
shop - go to shop
create_game - self-explorable
"""


class LobbyState(LobbyStateBase):

    def __init__(self, storage: StorageBase,
                 callback: Callable[[str, dict], None],
                 is_first_time: bool = True):
        super(LobbyState, self).__init__(storage, callback, is_first_time)
        self._COMMAND_FUNCS = {
            'help': self._show_help,
            'saves': self._show_saves,
            'load': self._load_save,
            'shop': self._go_to_shop,
            'create_game': self._create_game,
        }

        self.loop_running = True
        self.saved_games = self.storage.load_saved_games()
        threading.Thread(target=self._start_gui).start()

    def _start_gui(self):
        clrscr()
        if self.is_first_time:
            print_delayed(_MAN)
        print_delayed(_COMMANDS)
        while self.loop_running:
            print_delayed('Enter command:', end='')
            command = input()
            clrscr()
            try:
                slices = command.split()
                if len(slices) == 1:
                    self._COMMAND_FUNCS[slices[0]]()
                elif len(slices) > 1:
                    self._COMMAND_FUNCS[slices[0]](*slices[1:])
                else:
                    print_delayed("Wrong command", file=sys.stderr)
            except KeyError:
                print_delayed("Wrong command", file=sys.stderr)

    def _show_help(self):
        print_delayed(_COMMANDS)

    def _show_saves(self):
        if len(self.saved_games) == 0:
            print_delayed('You doesnt create saves before, sorry.')
        else:
            for idx, save in enumerate(self.saved_games):
                print_delayed(f'{idx}.', end='')
                print_delayed(f'{save.date}, {save.game_type}')

    def _load_save(self, idx):
        pass

    def _go_to_shop(self):
        self.callback(BackMessageFromLobby.ENTER_SHOP)
        self.loop_running = False

    def _create_game(self):
        self.callback(BackMessageFromLobby.CREATE_GAME)
        self.loop_running = False
