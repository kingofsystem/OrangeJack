import threading
from typing import Callable

import sys

import time

from db_observer import StorageBase
from game_objects import PlayerProfile
from gui import ShopStateBase
from .utils import print_delayed

_MAN = """
Imagine as you in beautiful shop of casino. You can buy everything you want 
(except some weird things). 
"""


class ShopState(ShopStateBase):
    def __init__(self, storage: StorageBase,
                 callback: Callable[[], None],
                 profile: PlayerProfile):
        super(ShopState, self).__init__(storage, callback, profile)
        threading.Thread(target=self._start_game).start()

    def _start_game(self):
        print_delayed(_MAN)
        while True:
            print_delayed('Hom many money you want to buy?', end='')
            money = input()
            if not money.isdigit():
                print_delayed('You entered a wrong number. Use only '
                              'digits.', file=sys.stderr)
            else:
                break
        print_delayed(f'Okay, you want to buy {money}$.')

        print_delayed('Please, enter your credit card number:', end='')
        card = input()

        print_delayed('Enter name on the card:', end='')
        name = input()

        print_delayed('Enter expiration date:', end='')
        date = input()

        print_delayed('Enter cvc:', end='')
        cvc = input()

        self.profile.money += int(money)
        self.storage.save_profile(self.profile)

        print_delayed('All information is correct!')
        time.sleep(1.5)

        self.callback()
