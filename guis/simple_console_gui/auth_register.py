import threading
from typing import Callable
from getpass import getpass
import sys

from db_observer import StorageBase
from game_objects import PlayerProfile
from gui import UserAuthRegisterStateBase
from guis.simple_console_gui.utils import clrscr, print_delayed


class UserAuthRegisterState(UserAuthRegisterStateBase):
    def __init__(self, storage: StorageBase,
                 callback: Callable[[PlayerProfile], None]):
        super().__init__(storage, callback)
        # Lol. May be threads is very dirty way here
        # TODO Make something with threads
        clrscr()
        threading.Thread(target=self._start_gui).start()

    def _start_gui(self):
        print_delayed('If you want to register, type reg, else type login'
                      ' [reg/LOGIN]: ', end='')
        choose = input().strip().lower()
        if choose == '' or choose == 'login':
            print_delayed('Please input your username and password...')
            username = input("Username: ")
            password = getpass("Password: ")

            try:
                user = self.storage.get_user(username)
            except Exception:
                self._restart_gui_auth()
                return
            if not user.check_pass(password):
                self._restart_gui_auth()
                return

            print_delayed('Everything is correct!')
            self.callback(user)
        elif choose == 'reg':
            print_delayed('Please input username and password of new '
                          'profile...')
            username = input("Username: ")
            password = getpass("Password: ")
            try:
                self.storage.get_user(username)
                print_delayed('User with this name already exists. Use'
                              ' another name.')
                self._start_gui()
                return
            except Exception:
                new_user = PlayerProfile(username, password)
                self.storage.save_profile(new_user)
                print_delayed('User successfully created!')
                self.callback(new_user)

    def _restart_gui_auth(self):
        print_delayed('Password or username is wrong, try again.',
                      file=sys.stderr)
        self._start_gui()
