import threading
from typing import List, Any, Callable

import sys

import time

from db_observer import StorageBase
from gui import CreateGameStateBase
from .utils import clrscr, print_delayed


class CreateGameState(CreateGameStateBase):
    def __init__(self, game_types: List[Any], storage: StorageBase,
                 callback: Callable[[dict], None]):
        super(CreateGameState, self).__init__(game_types, storage, callback)

        threading.Thread(target=self._start_game).start()

    def _start_game(self):
        clrscr()
        print_delayed('Here you can create game.')
        print_delayed('\nAvailable games:')
        for idx, game_type in enumerate(self.game_types):
            print_delayed(f'{idx}. {game_type.NAME}')

        while True:
            print_delayed('\nGet number of game:', end='')
            number = input()
            if not number.isdigit():
                print_delayed('Wrong input', file=sys.stderr)
            number = int(number)
            if 0 <= number < len(self.game_types):
                break
            else:
                print_delayed('Wrong number', file=sys.stderr)

        game_settings = {'game_type': self.game_types[number]}
        print_delayed('\nInput params for the game ')
        for param in self.game_types[number].SETTINGS:
            if param[0] == 'player':
                continue
            while True:
                print_delayed(param[2] + ':', end='')
                val = input()
                try:
                    val = param[1](val)
                    break
                except TypeError:
                    print_delayed('Wrong input!', file=sys.stderr)
                    continue
            game_settings[param[0]] = val

        print_delayed('All done. Creating game...')
        time.sleep(1.5)
        self.callback(game_settings)
