import hashlib
import uuid
from typing import List

from game_objects import GameResult, PlayerProfile, SavedGame


class StorageBase:

    def save_game(self, game_state: SavedGame):
        raise NotImplementedError()

    def load_saved_games(self) -> List[SavedGame]:
        raise NotImplementedError()

    def save_game_result(self, game_result: GameResult):
        raise NotImplementedError()

    def load_game_results(self) -> List[GameResult]:
        raise NotImplementedError()

    def save_profile(self, player_profile: PlayerProfile):
        raise NotImplementedError()

    def get_user(self, username: str) -> PlayerProfile:
        raise NotImplementedError()

    def get_user_by_id(self, user_id: str) -> PlayerProfile:
        raise NotImplementedError()

    def delete_user(self, profile_id: str) -> bool:
        raise NotImplementedError()

    def get_all_users(self) -> List[PlayerProfile]:
        raise NotImplementedError()

